// - create a function that accept single parameter. the parameter will be an array of integer
// - your function should be able to count the unique value inside the array
// - no googling
// - no open repo / practice
// - no build in function (sort, reduce, filter, etc)
// - examples:
//   - `countUniqueValues([1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13]))` => 7
//   - `countUniqueValues([1, 2, 2, 2, 2, 2, 2, 2, 4, 6]))` => 4
//   - `countUniqueValues([]))` => 0

function countUniqueValues(num){
    
if(!num.length){
    return 0;
}
// if(num.length == 1){
//     return 1;
// }

let x = 0 //1
let initialNum = 1

while(initialNum < num.length){
    if(num[x] !== num[initialNum]){
        x++;
        num[x] = num[initialNum]
    }    
    initialNum++;
}
    return x + 1;

}

console.log(countUniqueValues([1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13]))
console.log(countUniqueValues([1, 2, 2, 2, 2, 2, 2, 2, 4, 6]))
console.log(countUniqueValues([1]))
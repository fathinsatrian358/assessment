function averagePair (arr, num){
    let lSide = 0;
    let rSide =  arr.length - 1;

    while (lSide < rSide){
       let avg = (arr[lSide]+arr[rSide]/ 2)
       console.log(avg);
        if (avg === num) return true;
        else if (avg < num) lSide++;
        else rSide--;
    }
    return false
};


console.log(averagePair([1, 3, 3, 4, 6, 7, 9, 11, 12], 8));

//[2,5,7,8,9,12]